### SWADE White CyberPunk Theme

@JeansenVaars

##### (For Foundry VTT)

This module takes skeleton from @https://github.com/Tenyryas/swade-cyber-css

So thanks Tenyryas (@Kyane von Schnitzel#8654)

It is not published so you will have to download the zip here and extract it in Foundry's modules folder to enable it (Just place the `swade-white-cyber-css` folder)

Change the image url in the css to background-9 for a brigher more white clean version

![example](./example3.png)